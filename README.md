# Artificial Neural Network Module

Artificial Neural Network Module assignment for ECE 457b

Investigating neural network performance due to variability of dataset and network configuration.

<ul>
    <li><strong>Assignment1.pdf</strong> for assignment questions
    <li><strong>ECE457b_A1.DOCX</strong> for Assignment Report and written answers</li>
    <li> <strong>ArtificialNeuralNetworksAssignment.ipynb</strong>: jupyter notebook which explores neural network performances </li>
    <li><strong>Problem5_data.csv</strong> for multi-class dataset for training neural network classifier </li>
</ul>